# **Learning Process**

# I. How to learn faster with the Feynman Technique

## **1. What is the Feynman Technique?**

Feynman Technique is one of the effective study techniques where you teach what you're learning to someone else. It's a process of four steps:

- Take a piece of paper and write the concept's name on the top.
- Explaining the concept using simple language.
- Identifying problem areas and then going back to the sources to review.
- Pinpointing any complicated terms and challenging yourself to simplify them.

## **2. What are the different ways to implement this in your learning process?**

- Take notes of whatever I'm learning
- Try to explain them in simple language as we would explain them to a 5-year-old kid
- Try to find the areas where I'm having difficulty explaining

# II. Learning How to Learn TED talk by Barbara Oakley

## **3.Paraphrase the video in your own words.**

The video essentially talks about the ways one can learn how to learn effectively. It can be summarized in the following points:

- The Brain works in two modes: **Focus Mode** and **Diffused Mode**(a relaxed state of the brain). When we learn, we should be going back and forth between these modes to learn effectively.

- Procrastination is the act of lazing away when doing something that we'd rather not do. We feel physical pain in the part of our brain that analyzes pain. This pain is handled in two ways: either by procrastinating or by working a way through it. Procrastinating once or twice is not that big of a deal but doing this very often can make it kind of like an addiction.

- **Pomodoro Technique** is a way to handle procrastination where a timer is set for 25 minutes to focus on work. When the time is over, have some relaxed fun for a few minutes. Relaxation is an important part of the learning process.

- Never compare yourself with others in terms of thinking ability, understanding concepts, etc. This falls under illusions of competence in learning. Always focus on the work given and keep on testing yourself on those concepts to develop mastery in those concepts.

## **4.What are some of the steps that you can take to improve your learning process?**

- I should practice the Pomodoro Technique to prevent procrastination while doing some work.
- I should not compare myself with others while doing a task and should understand that each person has a unique experience when it comes to doing work.

# III. Learn Anything in 20 Hours

## **5. Your key takeaways from the video? Paraphrase your understanding.**

The video essentially says that we can learn any new skill that we are completely unaware, of if we put 20 hours of focussed, deliberate practice into the thing that we want to learn. The steps to achieve this rapid skill acquisition are explained below:

- **Deconstruct the skill**: Decide exactly what skill we want to develop and break it down into smaller pieces. The more we break apart the skill, we can decide which all parts of the skill are useful for us. Develop these parts first.

- **Learn Enough to Self-Correct**: Get 3 to 5 resources on the skill that we want to develop and practice just enough to self-correct the mistakes in practice.

- **Remove Practice Barriers**: Remove all distractions that hinder us to develop the skill.

- **Practice at least 20 hours**: We shouldn't get frustrated initially doing practice. Through constant practice only, we can develop a new skill.

## **6. What are some of the steps that you can take while approaching a new topic?**

Some of the steps I can take while developing a new skill can be listed as follows:

- Break the skill into smaller pieces and identify the parts that would help to develop the skill quickly
- Get enough resources to check my progress and don't use them for procrastination
- Remove all distractions that prevent me from practicing with the skill
- Practice at least 45min a day for a month to achieve at least 20 hours of practice time on the skill

## References

- https://www.youtube.com/watch?v=_f-qkGJBPts
- https://www.youtube.com/watch?v=O96fE1E-rf8
- https://www.youtube.com/watch?v=5MgBikgcWnY-
